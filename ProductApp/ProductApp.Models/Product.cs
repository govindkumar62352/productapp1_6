﻿namespace ProductApp.Models
{
    public class Product
    {
        #region Properties
        public int Id { get; set; }  
        public string? Name { get; set; }
        public string? Description { get; set; }
        public double Rating { get; set; } = 4.2;
        public bool IsAvailable { get; set; } = true;
        #endregion
        public override string ToString()
        {
            return $"{Id} \t{Name} \t{Description} \t{Rating}\t{IsAvailable}";
        }
    }
}
