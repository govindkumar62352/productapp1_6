﻿using ProductApp.Models;
using ProductApp.Repository;

//Object Creation

ProductRepository productRepo = new ProductRepository();
IProductRepository productRepository = (IProductRepository)productRepo;

Product product;
int counter = 0;
while (counter < 4)
{
    product = new Product();
    Console.Write("Product Id: : ");
    product.Id = Convert.ToInt32(Console.ReadLine());
    Console.Write("Product Name:: ");
    product.Name = Console.ReadLine();
    Console.Write("Prdouct Description : : ");
    product.Description = Console.ReadLine();
    bool productAddStatus = productRepository.AddProduct(product);
    counter++;

}

List<Product> productList = productRepository.GetALLProducts();
foreach (Product productobj in productList)
{
    Console.WriteLine(productobj);
}

Console.Write("Search Product By name: ");
string? productName = Console.ReadLine();
Product ? productDetailsByName = productRepository.GetProductByName(productName);
if (productDetailsByName != null)
{
    Console.WriteLine(productDetailsByName);
}
else
{
    Console.WriteLine("Product By name Not Found!! ");
}

Console.Write("Search Product By ID: ");
int productId = Convert.ToInt32(Console.ReadLine());
Product? productDetailsById = productRepository.GetProductById(productId);
if (productDetailsById != null)
{
    Console.WriteLine(productDetailsById);
}
else
{
    Console.WriteLine("Product By name Not Found!! ");
}
Console.WriteLine("Do You Want to Delete product YES|NO");
string? userYesOrNoSelection = Console.ReadLine();
if (userYesOrNoSelection != null)
{
    string userSlectionInupperCase = userYesOrNoSelection.ToUpper();
    Console.Write("Delete Product Name:: ");
    string ? productNameToDelete = Console.ReadLine();
    bool DeleteProductSatatus = productRepository.DeleteProductByName(productNameToDelete);
    if (DeleteProductSatatus)
    {
        Console.WriteLine("Product Delete Successfully");
    }
}
Console.WriteLine("---------------Final product Deatails---------------");
productRepository.GetALLProducts().ForEach(p =>
{
    global::System.Console.WriteLine(p);
});

Console.WriteLine("Update operation");
product = new Product()
{
    Id = 1,
    Name = "Lenvo",
    Description = "Lenvo Laptop"

};
bool productUpdatedStatus=productRepository.UpdateProduct(product);
if(productUpdatedStatus)
{
    Console.WriteLine("Update::Success");
}
else
{
    Console.WriteLine("update :Failure:");
}
Console.WriteLine("---------------Final product Deatails---------------");
productRepository.GetALLProducts().ForEach(p =>
{
    global::System.Console.WriteLine(p);
});

while (counter == 4)
{
    Console.WriteLine("Do you Want to Block the Project");
    Console.WriteLine("Do You Want to Delete product yes|no");
    userYesOrNoSelection = Console.ReadLine();
    if (userYesOrNoSelection != null)
    {
        string userSlectionInupperCase = userYesOrNoSelection.ToUpper();
        Console.Write("Block Product By Name :: ");
        string? productNameToBlock = Console.ReadLine();
        if (userSlectionInupperCase == "Yes")
        {
            bool blockUnblockStatus = productRepository.BlockUnblockProduct(productNameToBlock, true);
            if (blockUnblockStatus)
            {
                Console.WriteLine("sucesss");
            }
        }

    }
    counter --;
}
Console.WriteLine("---------------Final product Deatails---------------");
productRepository.GetALLProducts().ForEach(p =>
{
    global::System.Console.WriteLine(p);
});