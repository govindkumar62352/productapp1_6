﻿using ProductApp.Models;

namespace ProductApp.Repository
{
    public interface IProductRepository
    {
        bool AddProduct(Product product);
        List<Product> GetALLProducts();
        Product? GetProductByName(string? productName);
        Product? GetProductById(int productId);
        bool DeleteProductByName(string? productNameToDelete);
        bool UpdateProduct(Product product);
        bool BlockUnblockProduct(string? productNameToBlock, bool v);
    }
}
