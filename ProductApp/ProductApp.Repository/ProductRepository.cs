﻿using ProductApp.Models;

namespace ProductApp.Repository
{
    public class ProductRepository : IProductRepository
    {
        List<Product> _products;
        public ProductRepository()
        {
            _products = new List<Product>();
        }


        public bool AddProduct(Product product)
        {
            _products.Add(product);
            return true;

        }

        public bool BlockUnblockProduct(string? productNameToBlock, bool productAvailableStatus)
        {
            bool blockStatus = false;
            Product ? productDeatailsToBlock  = GetProductByName(productNameToBlock);
            if (productDeatailsToBlock != null)
            {
                productDeatailsToBlock.IsAvailable = productAvailableStatus;
                bool UpdateProductStatus=UpdateProduct(productDeatailsToBlock);
                blockStatus = UpdateProductStatus;
            }
            return blockStatus;
            
        }

        public bool DeleteProductByName(string? productNameToDelete)
        {
            bool productDeleteStatus = false;
            Product? ProductToDelete = GetProductByName(productNameToDelete);
            if (ProductToDelete != null)
            {
                productDeleteStatus=_products.Remove(ProductToDelete);
              
            }
            return productDeleteStatus;
        }


        public List<Product> GetALLProducts()
        {
            return _products.FindAll(p => p.IsAvailable == true). ToList();
        }

        public Product? GetProductById(int productId)
        {
            return _products.Find(p => p.Id == productId);
        }

        public Product GetProductByName(string? productName)
        {
#pragma warning disable CS8603 // Possible null reference return.
            return _products.Find(p => p.Name == productName);
#pragma warning restore CS8603 // Possible null reference return.

        }

        public bool UpdateProduct(Product product)
        {
            int productDetailsByIndex = _products.FindIndex(p => p.Id == product.Id);
            _products[productDetailsByIndex] = product;
            return true;
        }
    }
}
